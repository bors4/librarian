package HLib.WorkForm.ContPanels;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import HLib.WorkForm.ContPanels.EditMenu;
import HLib.WorkForm.ContPanels.FileMenu;
import HLib.WorkForm.ContPanels.HelpMenu;
import HLib.WorkForm.ContPanels.SearchMenu;

public class SystemMenu extends JMenuBar {
	public SystemMenu() {
		add(new FileMenu());
		add(new EditMenu());
		add(new SearchMenu());
		add(new HelpMenu());
	}
}

class FileMenu extends JMenu {
	public FileMenu() {
		super("File");
		JMenu newMenu = new JMenu("New");
		add(newMenu);
		JMenuItem newDoc = new JMenuItem("Document");
		newMenu.add(newDoc);
		JMenuItem openItem = new JMenuItem("Open");
		openItem.addActionListener(new ActionListener()
        {
           public void actionPerformed(ActionEvent event)
           {
              openFile();
           }
        });
		add(openItem);
		JMenuItem closeItem = new JMenuItem("Close");
		add(closeItem);
		JMenuItem closeAllItem = new JMenuItem("Close all");
		add(closeAllItem);
		JMenuItem save = new JMenuItem("Save");
		add(save);
		JMenuItem saveAs = new JMenuItem("Save As");
		add(saveAs);
		JMenuItem print = new JMenuItem("Print");
		add(print);
		addSeparator();
		JMenuItem exitItem = new JMenuItem("Exit");
		add(exitItem);
		exitItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});

	}
	public void openFile()
	   {
	      JFileChooser chooser = new JFileChooser();
	      chooser.setCurrentDirectory(new File("."));

	      chooser.setFileFilter(new javax.swing.filechooser.FileFilter()
	         {
	            public boolean accept(File f)
	            {
	               return f.isDirectory() || f.getName().toLowerCase().endsWith(".xml");
	            }

	            public String getDescription()
	            {
	               return "XML files";
	            }
	         });
	      int r = chooser.showOpenDialog(this);
	      if (r != JFileChooser.APPROVE_OPTION) return;
	      final File file = chooser.getSelectedFile();
	   }
}

class EditMenu extends JMenu {
	public EditMenu() {
		super("Edit");
		JMenuItem undoTyping = new JMenuItem("Undo Typing");
		add(undoTyping);
		JMenuItem redo = new JMenuItem("Redo");
		add(redo);
		JMenuItem cut = new JMenuItem("Cut");
		add(cut);
		JMenuItem copyMenu = new JMenuItem("Copy");
		add(copyMenu);
		JMenuItem change = new JMenuItem("Change");
		add(change);
		JMenuItem paste = new JMenuItem("Paste");
		add(paste);
		JMenuItem delete = new JMenuItem("Delete");
		add(delete);
		JMenuItem selectAll = new JMenuItem("selectAll");
		add(selectAll);
	}
}

class SearchMenu extends JMenu {
	public SearchMenu() {
		super("Search");
		JMenuItem searchInDoc = new JMenuItem("In Document");
		add(searchInDoc);
		JMenuItem searchInDB = new JMenuItem("In DB");
		add(searchInDB);
	}
}

class HelpMenu extends JMenu {
	public HelpMenu() {
		super("Help");
		JMenuItem Docs = new JMenuItem("Documentation");
		add(Docs);
		JMenuItem about = new JMenuItem("About");
		add(about);
	}
}