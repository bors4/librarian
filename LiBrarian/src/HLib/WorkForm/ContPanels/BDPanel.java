package HLib.WorkForm.ContPanels;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.Color;
import java.awt.Dimension;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

//Later create interface(abstract class) for DataTable

public class BDPanel extends JPanel {
	public BDPanel() {
		setBorder(BorderFactory.createLineBorder(Color.black));
		add(new JScrollPane());
		JTable table = new JTable(data, columnNames);
//		table height must be equals height of panel.
//		if height of table less height of panel then add a scroll 
		table.setRowHeight(20);
		System.out.println(table.getRowHeight());
		add(table);
		new DBConnection();
	}

	public Dimension getPreferredSize() {
		return new Dimension(900, 415);
	}

	private static String[] columnNames = { "Name", "Last modified", "Type",
			"Size" };
	private static String[][] data = {
			{ "addins", "02.11.2006 19:15", "Folder", "" },
			{ "AppPatch", "03.10.2006 14:10", "Folder", "" },
			{ "assembly", "02.11.2006 14:20", "Folder", "" },
			{ "Boot", "13.10.2007 10:46", "Folder", "" },
			{ "Branding", "13.10.2007 12:10", "Folder", "" },
			{ "Cursors", "23.09.2006 16:34", "Folder", "" },
			{ "Debug", "07.12.2006 17:45", "Folder", "" },
			{ "Fonts", "03.10.2006 14:08", "Folder", "" },
			{ "Help", "08.11.2006 18:23", "Folder", "" },
			{ "explorer.exe", "18.10.2006 14:13", "File", "2,93MB" },
			{ "helppane.exe", "22.08.2006 11:39", "File", "4,58MB" },
			{ "twunk.exe", "19.08.2007 10:37", "File", "1,08MB" },
			{ "nsreg.exe", "07.08.2007 11:14", "File", "2,10MB" },
			{ "avisp.exe", "17.12.2007 16:58", "File", "12,67MB" }, };
}

class DBConnection {
	public DBConnection(){
	String user = "root";
    String password = "*****";
    String url = "jdbc:mysql://localhost:3306/first_db";
    String driver = new String("com.mysql.jdbc.Driver");
    try {
         Class.forName(driver);
    } catch (ClassNotFoundException e1) {
         e1.printStackTrace();
    }
    Connection c = null;
    try{
         c = DriverManager.getConnection(url, user, password);
         Statement st = c.createStatement();
         System.out.println(c.getSchema());
         ResultSet rs = st.executeQuery("select * from DOCUMENTS");
         while(rs.next()){
              System.out.println(rs.getString("NAME"));
         }
    } catch(Exception e){
         e.printStackTrace();
    }
    finally{
         try {
              if(c != null)
              c.close();
         } catch (SQLException e) {         
              e.printStackTrace();
         }
    }
}}