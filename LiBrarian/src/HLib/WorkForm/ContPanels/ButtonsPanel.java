package HLib.WorkForm.ContPanels;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

public class ButtonsPanel extends JPanel {
	public ButtonsPanel() {
		setBorder(BorderFactory.createLineBorder(Color.black));
		setLayout(new GridLayout(3, 1, 5, 5));
		add(new CreateButton());
		add(new AddDocument());
	}

	public Dimension getPreferredSize() {
		return new Dimension(215, 200);
	}
}

class CreateButton extends JButton {
	public CreateButton() {
		super("Create new table");
		this.setSize(100, 200);

	}
}

class AddDocument extends JButton {
	public AddDocument() {
		super("Add new document");
	}
}
