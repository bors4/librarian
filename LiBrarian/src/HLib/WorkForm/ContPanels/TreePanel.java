package HLib.WorkForm.ContPanels;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;

public class TreePanel extends JPanel {

	public TreePanel() {
		// all data i'll need to get from XML file
		// and construct for their a tree of documents and nodes
		// to construct a tree, use DefaultMutableTreeNode
		setBorder(BorderFactory.createLineBorder(Color.black));
		Object[] data = new Object[] { "1", "2", "3",
				new String[] { "1/1", "2/2" }, new String[] { "1/1", "2/2" },
				new String[] { "1/1", "2/2" }, new String[] { "1/1", "2/2" },
				new String[] { "1/1", "2/2" }, new String[] { "1/1", "2/2" },
				new String[] { "1/1", "2/2" } };
		JTree tree = new JTree(data);
		JScrollPane sp = new JScrollPane(tree);
		// sp.setMaximumSize(this.getPreferredSize());
		this.add(sp);
		// File f = new File(".");
		// JTree tree = new JTree(new TreeDocuments(f));
		// add(tree);
	}

	// public Dimension getPreferredSize() {
	// return new Dimension(210, 380);
	// }
}

class FileTree {
	public FileTree() {

	}
}