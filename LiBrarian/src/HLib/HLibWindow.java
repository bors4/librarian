package HLib;

import javax.swing.*;

import HLib.WorkForm.MainForm;
import HLib.WorkForm.ContPanels.BDPanel;
import HLib.WorkForm.ContPanels.ButtonsPanel;
import HLib.WorkForm.ContPanels.SystemMenu;
import HLib.WorkForm.ContPanels.TreePanel;

public class HLibWindow extends JFrame {
	public HLibWindow() {
		JFrame frame = new JFrame();

		frame.setVisible(true);
		frame.setSize(1180, 700);
		frame.setTitle("Home library");
		frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.add(new WorkPanel());
	}
}

class WorkPanel extends JPanel {
	public WorkPanel() {
		MainForm mf = new MainForm();
		SystemMenu sm = new SystemMenu();
		ButtonsPanel bp = new ButtonsPanel();
		TreePanel tp = new TreePanel();
		BDPanel bdp = new BDPanel();
		add(sm);
		add(mf);
		add(bp);
		add(tp);
		add(bdp);
		SpringLayout layout = new SpringLayout();
		setLayout(layout);
		layout.putConstraint(SpringLayout.WEST, sm, 0, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.NORTH, mf, 20, SpringLayout.NORTH, sm);
		layout.putConstraint(SpringLayout.NORTH, tp, 10, SpringLayout.NORTH, mf);
		layout.putConstraint(SpringLayout.WEST, tp, 5, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.NORTH, bp, 420, SpringLayout.NORTH, tp);
		layout.putConstraint(SpringLayout.WEST, bp, 5, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.WEST, bdp, 210, SpringLayout.NORTH, tp);
		layout.putConstraint(SpringLayout.NORTH, bdp, 0, SpringLayout.NORTH, tp);
	}
}