package Application.UI;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ELib.ELibWindow;
import HLib.HLibWindow;

public class FormChoiceMode extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// ��������� ���� ������
	public FormChoiceMode() {
		setVisible(true);
		setTitle("Choise mode");
		setLocation(400, 150);
		setSize(350, 100);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	{
		JPanel p1 = new JPanel();
		p1.setLayout(new FlowLayout());
		this.add(p1);
		p1.add(new JLabel("You have to choise mode to work."));
		p1.add(new JLabel("Press Ok to continue"));
		JButton bOk = new JButton("Ok");
		JButton bClose = new JButton("Close");
		p1.add(bOk);
		p1.add(bClose);
		final JComboBox<String> box = new JComboBox<String>();
		p1.add(box);
		bOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if (box.getSelectedItem() == "Home library") {
					try {
						finalize();
					} catch (Throwable e) {
						e.printStackTrace();
					}
					new HLibWindow();
				} else {
					new ELibWindow();
				}
				dispose();
			}
		});
		box.addItem("Home library");
		box.addItem("Electronic documentation");
		p1.add(box);
		this.setResizable(false);
	}
}